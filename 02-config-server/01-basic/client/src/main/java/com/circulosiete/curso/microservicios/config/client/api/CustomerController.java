package com.circulosiete.curso.microservicios.config.client.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RefreshScope
@RestController
@RequestMapping("/v1/customers")
public class CustomerController {
  private final String message;

  public CustomerController(@Value("${application.message:Hola}") String message) {
    this.message = message;
  }

  @GetMapping
  public Map<String, String> foo() {
    return Map.of("message", message);
  }
}
