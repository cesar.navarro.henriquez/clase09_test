package com.circulosiete.curso.microservicios.messaging.rabbit.web;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import static com.circulosiete.curso.microservicios.messaging.rabbit.config.RabbitConfig.exchangeIntroRabbitMQ;
import static com.circulosiete.curso.microservicios.messaging.rabbit.config.RabbitConfig.routingKey;
import static org.springframework.http.ResponseEntity.ok;

@RestController
public class ProducerController {
  public static final String MESSAGE = "Hola desde RabbitMQ!";
  private final RabbitTemplate rabbitTemplate;

  public ProducerController(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }

  @PostMapping("/queue/simple")
  public HttpEntity queueSimple() {
    System.out.println("Recibiendo mensage: " + new Date());
    rabbitTemplate.convertAndSend(exchangeIntroRabbitMQ, routingKey, MESSAGE);

    return ok(true);
  }

  @PostMapping("/queue/header")
  public HttpEntity queueHeader() {
    rabbitTemplate.convertAndSend(exchangeIntroRabbitMQ, routingKey, MESSAGE, message -> {
      message.getMessageProperties()
        .setHeader("index", "1");
      return message;
    });

    return ok(true);
  }
}
