package com.circulosiete.curso.microservicios.messaging.rabbit.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
  public final static String queueName = "queueIntroRabbitMQ";
  public final static String routingKey = queueName;
  public final static String exchangeIntroRabbitMQ = "exchangeIntroRabbitMQ";

  @Bean
  Queue queue() {
    return new Queue(queueName);
  }

  @Bean
  TopicExchange exchange() {
    return new TopicExchange(exchangeIntroRabbitMQ);
  }

  @Bean
  Binding binding(Queue queue, TopicExchange exchange) {
    return BindingBuilder
      .bind(queue)
      .to(exchange)
      .with(routingKey);
  }
}

