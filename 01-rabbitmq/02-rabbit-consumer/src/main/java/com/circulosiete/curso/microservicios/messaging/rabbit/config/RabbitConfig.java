package com.circulosiete.curso.microservicios.messaging.rabbit.config;

import com.circulosiete.curso.microservicios.messaging.rabbit.listener.MessagesListener;
import com.rabbitmq.client.Consumer;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
  public final static String queueName = "queueIntroRabbitMQ";
  public final static String routingKey = queueName;
  public final static String exchangeIntroRabbitMQ = "exchangeIntroRabbitMQ";

  @Bean
  Queue queue() {
    return new Queue(queueName);
  }

  @Bean
  TopicExchange exchange() {
    return new TopicExchange(exchangeIntroRabbitMQ);
  }

  @Bean
  Binding binding(Queue queue, TopicExchange exchange) {
    return BindingBuilder
      .bind(queue)
      .to(exchange)
      .with(routingKey);
  }

  @Bean
  SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                           MessageListenerAdapter listenerAdapter) {
    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    container.setConnectionFactory(connectionFactory);
    container.setQueueNames(queueName);
    container.setMessageListener(listenerAdapter);
    return container;
  }

  @Bean
  MessageListenerAdapter listenerAdapter(MessagesListener messagesListener) {
    return new MessageListenerAdapter(messagesListener, "listen");
  }

}

