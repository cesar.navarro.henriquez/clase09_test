package com.circulosiete.curso.microservicios.messaging.rabbit.listener;


import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Date;

import static com.circulosiete.curso.microservicios.messaging.rabbit.config.RabbitConfig.queueName;
import static java.lang.String.format;

@RefreshScope
@Service
public class MessagesListener {

  private final int duration;

  public MessagesListener(@Value("${listenerMaxDuration:7000}") int duration) {
    this.duration = duration;
  }

  //@RabbitListener(queues = queueName)
  public void listen(String  body) {
    StringBuilder sb = new StringBuilder();
    sb.append("=================================================\n");
    sb.append("duration:");
    sb.append(duration);
    sb.append("\n");
    //String body = format("%s %s", new String(message.getBody()), new Date());
    sb.append(body);
    sb.append("\n");
    //sb.append(message.getMessageProperties().toString());
    sb.append("\n=================================================\n");

    try {
      Thread.sleep(SecureRandom.getInstanceStrong().nextInt(duration));
    } catch (Throwable e) {
      e.printStackTrace();
    }
    System.out.println(sb.toString());
  }
}